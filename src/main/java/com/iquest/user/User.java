package com.iquest.user;

/**
 * Class used to store information on the client which successfully logs in.
 * 
 * @author andrei.georgescu
 *
 */
public class User {
	/**
	 * The login name of the client.
	 */
	private String userName;
	/**
	 * The login password of the client.
	 */
	private String userPassword;

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserName() {
		return userName;
	}

	public String getUserPassword() {
		return userPassword;
	}
}
