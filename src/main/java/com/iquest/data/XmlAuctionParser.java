package com.iquest.data;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.iquest.auctions.AuctionItem;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class XmlAuctionParser {
	private Document parsedXml;

	public XmlAuctionParser(String filename)
			throws ParserConfigurationException, URISyntaxException,
			SAXException, IOException {
		File file = new File(this.getClass().getResource(filename).toURI());

		DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
				.newDocumentBuilder();
			this.parsedXml = dBuilder.parse(file);
		
	}

	public List<AuctionItem> getAuctions() {
		List<AuctionItem> auctions = new ArrayList<AuctionItem>();

		NodeList nodes = parsedXml.getElementsByTagName("auctions");
		Element element = (Element) nodes.item(0);

		NodeList auctionList = element.getElementsByTagName("auction");

		for (int i = 0; i < auctionList.getLength(); i++) {
			AuctionItem auction = new AuctionItem();
			Element auctionElement = (Element) auctionList.item(i);

			auction.setProductName(auctionElement
					.getElementsByTagName("product-name").item(0)
					.getTextContent());
			auction.setDescription(auctionElement
					.getElementsByTagName("descriptions").item(0)
					.getTextContent());
			auction.setBid(Integer.parseInt(auctionElement
					.getElementsByTagName("starting-bid").item(0)
					.getTextContent()));
			auction.setBuyout(Integer.parseInt(auctionElement
					.getElementsByTagName("buyout-price").item(0)
					.getTextContent()));

				auctions.add(auction);
		}
		return auctions;
	}

}
