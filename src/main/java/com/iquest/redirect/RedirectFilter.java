package com.iquest.redirect;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Class used to redirect any client request which tries to access resources
 * without him being logged in.
 * 
 * @author andrei.georgescu
 *
 */
public class RedirectFilter implements Filter {
	/**
	 * The configuration object used to get the ignored URLs which correspond to
	 * the Login servlets and jsps.
	 */
	FilterConfig config;

	/**
	 * Method used to get the filter's configuration object.
	 */
	@Override
	public void init(FilterConfig config) throws ServletException {
		this.config = config;
	}

	/**
	 * Method used to invalidate requests which come from a session-less client
	 * and redirect them to the login page.
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		String requestPath = ((HttpServletRequest) request).getRequestURL()
				.toString();

		if (checkIgnoredPath(requestPath)) {
			chain.doFilter(request, response);
			return;
		}

		HttpSession sessionStarted = ((HttpServletRequest) request)
				.getSession(false);

		if (!checkSession(sessionStarted)) {
			((HttpServletResponse) response).sendRedirect("Login.jsp");
		} else {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void destroy() {
	}

	/**
	 * Method used to check if the request corresponds to the login page.
	 * 
	 * @param path
	 *            The path which comes from the client request.
	 * @return A boolean which is true if the request path is one of the ignored
	 *         paths and false otherwise.
	 */
	private boolean checkIgnoredPath(String path) {
		Enumeration<String> ignoredRequests = config.getInitParameterNames();

		String ignoredPath = " ";
		while (ignoredRequests.hasMoreElements()) {
			ignoredPath = ignoredRequests.nextElement();
			if (path.equals(config.getInitParameter(ignoredPath))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Method used to see if there exists a session for the current client.
	 * 
	 * @param session
	 *            The session which is going to be checked.
	 * @return A boolean which is true if there is a session created for the
	 *         client and false otherwise.
	 */
	private boolean checkSession(HttpSession session) {
		if (session == null) {
			return false;
		}
		return true;
	}
}
