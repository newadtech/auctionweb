package com.iquest.auctions;

/**
 * Class used to store the information of an item that is going to be sold in an
 * auction.
 * 
 * @author andrei.georgescu
 *
 */
public class AuctionItem {
	/**
	 * 
	 * The name of the item.
	 */
	private String productName;
	/**
	 * Small information regarding the item.
	 */
	private String description;
	/**
	 * The current bid price for the item.
	 */
	private Integer bid;
	/**
	 * The buyout price of the item.
	 */
	private Integer buyout;
	/**
	 * The currency for the item's prices.
	 */
	private String currency;
	/**
	 * The name of the user who has bid last.
	 */
	private String currentBidder;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getBid() {
		return bid;
	}

	public void setBid(Integer bid) {
		this.bid = bid;
	}

	public Integer getBuyout() {
		return buyout;
	}

	public void setBuyout(Integer buyout) {
		this.buyout = buyout;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCurrentBidder() {
		return currentBidder;
	}

	public void setCurrentBidder(String currentBidder) {
		this.currentBidder = currentBidder;
	}

}
