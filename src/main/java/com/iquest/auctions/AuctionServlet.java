package com.iquest.auctions;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.iquest.data.XmlAuctionParser;

/**
 * Class used to get the information from the xml which contains all of the
 * auctions which are currently available.
 * 
 * @author andrei.georgescu
 *
 */
public class AuctionServlet extends HttpServlet {

	/**
	 * Method used to get the auctions from an xml and then send the data
	 * througha jsp to the user.
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		List<AuctionItem> auctions = new ArrayList<AuctionItem>();
		try {
			XmlAuctionParser parser = new XmlAuctionParser("/auctions.xml");
			auctions = parser.getAuctions();

		} catch (ParserConfigurationException | URISyntaxException
				| SAXException e) {

			new IllegalStateException(e);
		}

		request.setAttribute("auctions", auctions);
		request.getRequestDispatcher("AuctionList.jsp").include(request,
				response);
	}

}
