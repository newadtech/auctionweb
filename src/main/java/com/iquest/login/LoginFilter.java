package com.iquest.login;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Class used to validate if the input given by the client in login form is
 * valid or not.
 * 
 * @author andrei.georgescu
 *
 */
public class LoginFilter implements Filter {

	@Override
	public void init(FilterConfig config) throws ServletException {
	}

	/**
	 * Method used to check whether or not the user login information is valid
	 * or not. In case it is, the login process will continue and the client
	 * will receive his user page in the response. If this is not the case, then
	 * he will be redirected to the login page.
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		String userName = request.getParameter("username");
		String userPassword = request.getParameter("userpass");

		if (!loginCheck(userName, userPassword)) {
			RequestDispatcher reqDispatcher = request
					.getRequestDispatcher("Login.jsp");

			response.setContentType("text/html");
			response.getWriter().println("Invalid username or password!");

			reqDispatcher.include(request, response);

		} else {
			chain.doFilter(request, response);
		}
	}

	/**
	 * Method used to check whether the username and password of the client are
	 * valid.
	 * 
	 * @param userName
	 *            The login name which corresponds to the client which sends the
	 *            request.
	 * @param userPassword
	 *            The login password which corresponds to the client which sends
	 *            the request.
	 * @return
	 */
	private boolean loginCheck(String userName, String userPassword) {
		if (userName == null || userPassword == null) {
			return false;
		}
		return true;
	}

	@Override
	public void destroy() {

	}
}
