package com.iquest.login;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.iquest.user.User;

/**
 * Class used for creating a user session.
 * 
 * @author andrei.georgescu
 *
 */
public class Login extends HttpServlet {
	/**
	 * Method used to set the username and password on his session, based on his
	 * login information.
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		User data = new User();
		data.setUserName(request.getParameter("username"));
		data.setUserPassword(request.getParameter("userpass"));

		HttpSession userSession = request.getSession(true);
		userSession.setAttribute("user", data);
		try {
			if (!checkUserInDatabase(data.getUserName(), data.getUserPassword())) {
				redirectToLogin("Invalid username or password", request,
						response);
			} else {

				request.getRequestDispatcher("Userpage.jsp").include(request,
						response);
			}
		} catch (XMLStreamException e) {
			e.printStackTrace();
		}

	}

	private boolean checkUserInDatabase(String userName, String password)
			throws XMLStreamException, FileNotFoundException {
		String filePath = new String("D:\\DatabaseXml\\userdata.xml");

		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader streamReader = factory
				.createXMLStreamReader(new BufferedInputStream(
						new FileInputStream(filePath)));

		while (streamReader.hasNext()) {
			streamReader.next();

			if (streamReader.getEventType() == XMLStreamReader.START_ELEMENT) {
				String nodeName = streamReader.getLocalName();

				if ("id".equals(nodeName)) {
					if (userName.equals(streamReader.getElementText())) {
						return true;
					}
					if ("password".equals(nodeName)) {
						if (password.equals(streamReader.getElementText())) {
							return true;
						}
					}
					break;

				}
			}
		}
		return false;
	}

	private void redirectToLogin(String message, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");

		PrintWriter printer = response.getWriter();
		printer.println(message);

		RequestDispatcher reqDispatch = request
				.getRequestDispatcher("Login.jsp");
		reqDispatch.include(request, response);

	}

}