package com.iquest.registration;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.xml.sax.SAXException;

public class Register extends HttpServlet {
	private String userName;
	private String userPassword;
	private String email;
	private String firstname;
	private String lastname;

	boolean userTagReached;
	boolean emailTagReached;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		userName = request.getParameter("username");
		userPassword = request.getParameter("userpass");
		email = request.getParameter("email");
		firstname = request.getParameter("firstname");
		lastname = request.getParameter("lastname");

		try {
			if (!createUser()) {
				response.setContentType("text/html");

				PrintWriter printer = response.getWriter();
				printer.println("Username or email already in use!");

			} else {
				redirectToLogin("You have successfully created your account!",
						request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public boolean createUser() throws ParserConfigurationException,
			SAXException, IOException, XMLStreamException {

		String filePath = new String("D:\\DatabaseXml\\userdata.xml");
		StringBuffer modifiedXml = new StringBuffer();

		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader streamReader = factory
				.createXMLStreamReader(new BufferedInputStream(
						new FileInputStream(filePath)));

		while (streamReader.hasNext()) {
			streamReader.next();

			switch (streamReader.getEventType()) {
			case XMLStreamReader.START_DOCUMENT:
				modifiedXml
						.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				break;
			case XMLStreamReader.START_ELEMENT:
				String nodeName = streamReader.getLocalName();

				if ("id".equals(nodeName)) {
					userTagReached = true;
				}
				if ("email".equals(nodeName)) {
					emailTagReached = true;
				}

				modifiedXml.append("<" + nodeName + ">");
				break;
			case XMLStreamReader.CHARACTERS:
				if (checkDuplicateUser(streamReader.getText())) {
					streamReader.close();
					return false;
				}

				modifiedXml.append(streamReader.getText());
				break;

			case XMLStreamReader.END_ELEMENT:
				if ("users-data".equals(streamReader.getLocalName())) {
					addUserData(modifiedXml);
				}
				modifiedXml.append("</" + streamReader.getLocalName() + ">");
				break;
			}
		}
		updateXml(modifiedXml);
		return true;
	}

	private void addUserData(StringBuffer modifiedXml) {

		modifiedXml.append("<user>");
		modifiedXml.append(System.getProperty("line.separator"));

		modifiedXml.append("<id>" + userName + "</id>");
		modifiedXml.append(System.getProperty("line.separator"));

		modifiedXml.append("<password>" + userPassword + "</password>");
		modifiedXml.append(System.getProperty("line.separator"));

		modifiedXml.append("<email>" + email + "</email>");
		modifiedXml.append(System.getProperty("line.separator"));

		modifiedXml.append("<firstname>" + firstname + "</firstname>");
		modifiedXml.append(System.getProperty("line.separator"));

		modifiedXml.append("<lastname>" + lastname + "</lastname>");
		modifiedXml.append(System.getProperty("line.separator"));

		modifiedXml.append("</user>");
		modifiedXml.append(System.getProperty("line.separator"));

	}

	private void redirectToLogin(String message, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");

		PrintWriter printer = response.getWriter();
		printer.println(message);

		RequestDispatcher reqDispatch = request
				.getRequestDispatcher("Login.jsp");
		reqDispatch.include(request, response);

	}

	private boolean checkDuplicateUser(String userData) {
		if (userTagReached) {
			if (userData.equals(userName)) {
				return true;
			}
		}
		if (emailTagReached) {
			if (userData.equals(email)) {
				return true;
			}
		}
		return false;
	}

	private void updateXml(StringBuffer userData) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(new File(
				"D:\\DatabaseXml\\userdata.xml")));

		writer.write(userData.toString());
		writer.close();

	}
}
