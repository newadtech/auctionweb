package com.iquest.logout;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class used to logout an user.
 * 
 * @author andrei.georgescu
 *
 */
public class Logout extends HttpServlet {
	/**
	 * Method used to invalidate the session of an user and redirect him to the
	 * homepage.
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.getSession().invalidate();

		request.getRequestDispatcher("Login.jsp").forward(request, response);
	}

}
