<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.iquest.auctions.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table border="1" style="width: 100%">
		<tr>
			<th>Auction name</th>
			<th>Description</th>
			<th>Bid</th>
			<th>Buyout</th>
		</tr>

		<c:forEach var="auction" items="${requestScope.auctions}">
			<tr>
				<td><c:out value="${auction.getProductName()}" /></td>
				<td><c:out value="${auction.getDescription()}" /></td>
				<td><c:out value="${auction.getBid()}" /></td>
				<td><c:out value="${auction.getBuyout()}" /></td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>